package com.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.management.Exception.CustomException;
import com.management.Repository.EmployeeRepository;
import com.management.model.Employee;

@CrossOrigin(origins = "http://localhost:4200/**")
@RestController
@RequestMapping("/api/employees/")
public class EmployeeController {

	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@PostMapping("/")
	public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee)
	{
		employeeRepository.save(employee);
		return new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
	}
	
	@GetMapping("/")
	public ResponseEntity<List<Employee>> findAllEmployee(){
		List<Employee> employees= employeeRepository.findAll();
		return new ResponseEntity<List<Employee>>(employees,HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public Employee getEmployeeById(@PathVariable(value = "id") long id) {
		return employeeRepository.findById(id).orElseThrow(() -> new CustomException("Employee not found with id : "+id));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") long id, @RequestBody Employee employee){
		Employee currentEmployee = employeeRepository.findById(id).orElseThrow(() -> new CustomException("Employee not found with id : "+id));
		currentEmployee.setFirstName(employee.getFirstName());
		currentEmployee.setLastName(employee.getLastName());
		currentEmployee.setAddress(employee.getAddress());
		
		employeeRepository.saveAndFlush(currentEmployee);
		return new ResponseEntity<Employee>(currentEmployee,HttpStatus.OK);
		
	}
	
	@DeleteMapping("/{id}")
	public String deleteEmployeeById(@PathVariable(value = "id") long id) {
		Employee emp = employeeRepository.findById(id).orElseThrow(() -> new CustomException("Employee not found with id : "+id));
		employeeRepository.delete(emp);
		return "Employee deleted successfully";
	}
}
