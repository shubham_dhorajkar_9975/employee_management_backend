package com.management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.management.model.LoginBody;

@RestController
@RequestMapping("/api/auth/")
public class AuthController {

	@Autowired
	private UserDetailsService detailsService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@PostMapping("/")
	public int login(@RequestBody LoginBody loginBody)
	{
		try {
			UserDetails user = detailsService.loadUserByUsername(loginBody.getUsername());
			return 1;
			
		} catch (Exception e) {
			return 0;
		}
		
	}
	
}
