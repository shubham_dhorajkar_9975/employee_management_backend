package com.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.management.Repository.ManagerRepository;
import com.management.model.Manager;


@RestController
@RequestMapping("/api/manager/")
public class ManagerController {

	@Autowired
	private ManagerRepository managerRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	@PostMapping("/")
	public ResponseEntity<Manager> createManager(@RequestBody Manager manager)
	{
		manager.setPassword(passwordEncoder.encode(manager.getPassword()));
		managerRepository.save(manager);
		return new ResponseEntity<Manager>(manager, HttpStatus.CREATED);
	}
	@GetMapping("/")
	public ResponseEntity<List<Manager>> getAllManager() {
		List<Manager> managers = managerRepository.findAll();
		return new ResponseEntity<List<Manager>>(managers, HttpStatus.OK);
	}
}
