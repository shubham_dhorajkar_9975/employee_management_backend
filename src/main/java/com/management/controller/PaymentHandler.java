package com.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.management.Repository.PaymentRepo;
import com.management.model.Payment;

@CrossOrigin(origins = "http://localhost:4200/**")
@RestController
@RequestMapping("/api/paymentHandler/")
public class PaymentHandler {

	@Autowired
	private PaymentRepo paymentRepository;
	
	@GetMapping("/")
	public ResponseEntity<List<Payment>> findAllPayment(){
		List<Payment> payments = paymentRepository.findAll();
		return new ResponseEntity<List<Payment>>(payments,HttpStatus.OK);
	}
}
