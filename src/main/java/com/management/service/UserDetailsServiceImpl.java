package com.management.service;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import com.management.Repository.ManagerRepository;
import com.management.model.Manager;

@Service
class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private ManagerRepository managerRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			Manager manager = managerRepository.findByUsername(username);
			return new org.springframework.security.core.userdetails.User(manager.getUsername(), manager.getPassword(), true , true , true , true ,getAuthorities("ROLE_USER"));
		} catch (Exception e) {
			// TODO: handle exception
			throw new UsernameNotFoundException("User is not found");
		}
		
	}

	private Collection<? extends GrantedAuthority> getAuthorities(String role_user) {
		// TODO Auto-generated method stub
		return Collections.singletonList(new SimpleGrantedAuthority(role_user));
	}
	
}
