package com.management.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.management.model.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long>{

	Manager findByUsername(String username);

	@Query("FROM Manager WHERE username=?1")
	Manager findManagerByUsername(String username);

}
