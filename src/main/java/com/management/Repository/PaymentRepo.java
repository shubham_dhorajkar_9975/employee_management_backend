package com.management.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.management.model.Payment;

@Repository
public interface PaymentRepo extends JpaRepository<Payment, Integer> {
	
	Payment findByTxnId(String txnId);

}
